# Modus themes (Modus Operandi and Modus Vivendi)

A pair of highly accessible themes that conform with the WCAG AAA
standard for colour contrast between background and foreground colour
combinations.

The themes are built into GNU Emacs (Emacs' git `master` branch, aka
version `28.0.50`).  They are also distributed in several packages
formats.

+ 'modus-operandi' is light
+ 'modus-vivendi' is dark

## Further information

Read the [Info manual HTML](https://protesilaos.com/modus-themes)
version for how to install, load, enable, and customise the themes.

If you are using the latest version of the themes from this repository's
`master` branch (e.g. the MELPA packages) or version >= `0.13.0`, the
manual is already installed on your machine: evaluate `(info
"(modus-themes) Top")` to start reading it.

The themes cover a lot of packages and are highly customisable.

For some demo content, check:

+ the screenshots https://protesilaos.com/modus-themes-pictures/
+ my videos https://protesilaos.com/code-casts/
